(define-module (dissoc packages gps)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages base)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages fontutils)
  #:use-module (gnu packages fribidi)
  #:use-module (gnu packages game-development)
  #:use-module (gnu packages gd)
  #:use-module (gnu packages gettext)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gnome)
  #:use-module (gnu packages gps)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages icu4c)
  #:use-module (gnu packages image)
  #:use-module (gnu packages java)
  #:use-module (gnu packages jemalloc)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages pcre)
  #:use-module (gnu packages pkg-config)
  #:use-module (gnu packages python)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages speech)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages xorg)
  #:use-module (guix build utils)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system gnu)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix packages))

;; NOTE: the packages in this module were some of the first I wrote

(define-public shapelib
  (package
   (name "shapelib")
   (version "1.5.0")
   (source (origin
            (method git-fetch)
            (uri (git-reference (url "https://github.com/OSGeo/shapelib.git")
                                (commit (string-append "v" version))) )
            (sha256
             (base32
              "1lzch0jf6yqhw391phhafzw4ghmiz98zkf698h4fmq109fa2vhqd"))))
   (build-system gnu-build-system)
   (native-inputs
    `(("autoconf" ,autoconf)
      ("automake" ,automake)
      ("libtool" ,libtool)))
   (synopsis "C Library for reading, writing and updating ESRI Shapefiles")
   (description "The Shapefile C Library provides the ability to write simple C programs for reading, writing and updating (to a limited extent) ESRI Shapefiles, and the associated attribute file (.dbf).")
   (home-page "http://shapelib.maptools.org/")
   (license license:gpl2)))

(define-public navit
  (package
   (name "navit")
   (version "0.5.5")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url "https://github.com/navit-gps/navit.git")
                  (commit (string-append "v" version))))
            (sha256
             (base32 "0kdq4a1zlyq3w6rpmvbz8k71ljzn0f1llsn6msr59z4lsqyzyy2m"))
            (file-name (git-file-name name version))))
   (build-system cmake-build-system)
   (arguments
    '(#:configure-flags '("-DCMAKE_BUILD_TYPE=Release"
                          "-DSAMPLE_MAP=n "
                          "-Dspeech/qt5_espeak=FALSE"
                          "-Dsupport/espeak=FALSE")

      #:tests? #f
      #:phases
      (modify-phases %standard-phases
                     (add-after 'install 'sample-map
                                (λ _
                                  #f)))))
   (native-inputs `(("bzip2" ,bzip2)
                    ("pkgconfig" ,pkg-config)))
   (inputs `(("atk" ,atk)
             ("cairo" ,cairo)
             ("dbus" ,dbus)
             ("dbus-glib" ,dbus-glib)
             ("espeak" ,espeak)
             ("fontconfig" ,fontconfig)
             ("freeglut" ,freeglut)
             ("freeimage" ,freeimage)
             ("freetype" ,freetype)
             ("fribidi" ,fribidi)
             ("gd" ,gd)
             ("gdk-pixbuf" ,gdk-pixbuf)
             ("gettext" ,gettext-minimal)
             ("glib" ,glib)
             ("glu" ,glu)
             ("gtk2" ,gtk+-2)
             ("imlib2" ,imlib2)
             ("librsvg" ,librsvg)
             ("libxmu" ,libxmu)
             ("libxslt" ,libxslt)
             ("mesa" ,mesa)
             ("pango" ,pango-1.42)
             ("pcre" ,pcre)
             ("python" ,python)
             ("quesoglc" ,quesoglc)
             ("shapelib" ,shapelib)
             ("speech-dispatcher" ,speech-dispatcher)
             ;; for qt. probably not needed
             ("qtquickcontrols" ,qtquickcontrols)
             ("qtmultimedia" ,qtmultimedia)
             ("qtspeech" ,qtspeech)
             ("qtsensors" ,qtsensors)
             ("qtbase" ,qtbase)
             ("qtlocation" ,qtlocation)
             ("qtdeclarative" ,qtdeclarative)
             ("qtsvg" ,qtsvg)))
   (synopsis "Navit is a open source (GPL) car navigation system with routing engine.")
   (description "Navit is a free and open-source, modular, touch screen friendly, car navigation system with GPS tracking, realtime routing engine and support for various vector map formats. It features both a 2D and 3D view of map data.")
   (home-page "https://www.navit-project.org/")
   (license license:gpl2)))

(define-public marble
  (package
   (name "marble")
   (version "20.08.1")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url "https://invent.kde.org/education/marble.git")
                  (commit (string-append "v" version))))
            (sha256
             (base32
              "0kdq4a1zlyq3w6rpmvbz8k71ljzn0f1llsn6msr59z4lsqyzyy2m"))
            (file-name (git-file-name name version))))
   (build-system cmake-build-system)
   (arguments
    '(#:configure-flags '("-DWITH_KF5=OFF"
                          "-DWITH_libwlocate=OFF")
      #:tests? #f))
   (propagated-inputs `(("gpsd" ,gpsd)))
   (inputs `(("qtbase" ,qtbase)
             ("qtdeclarative" ,qtdeclarative)
             ("qtmultimedia" ,qtmultimedia)
             ("qtquickcontrols" ,qtquickcontrols)
             ("qtscript" ,qtscript)
             ("qtsvg" ,qtsvg)
             ("qttools" ,qttools)
             ("qtwebengine" ,qtwebengine)))
   (synopsis "Marble is a virtual globe and world atlas — your swiss army knife for maps.")
   (description "Versatile, yet easy to use. Use Marble similar to a desktop globe; pan around and measure distances. At closer scale it becomes a world atlas, while OpenStreetMap takes you to street level. Search for places of interest, view Wikipedia articles, create routes by drag and drop and so much more.")
   (home-page "https://marble.kde.org/")
   (license license:lgpl2.1)))
