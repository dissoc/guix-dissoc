(define-module (dissoc packages chromium)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages base)
  #:use-module (gnu packages chromium)
  #:use-module (gnu packages gtk)
  #:use-module (gnu packages pcre)
  #:use-module (gnu packages pkg-config)
  #:use-module (guix build utils)
  #:use-module (guix build-system gnu)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix packages)
  #:use-module (guix utils))


;; This is an attempt to make chromium less detectable when using
;; chromedriver.

(define-public ungoogled-chromium/undetectable
  (package/inherit ungoogled-chromium
                   (name "ungoogled-chromium-undetectable")
                   (inputs
                    `(,@(package-inputs ungoogled-chromium)))
                   (arguments
                    (substitute-keyword-arguments
                     (package-arguments ungoogled-chromium)
                     ((#:phases phases)
                      `(modify-phases
                        ,phases
                        (add-after 'add-absolute-references 'patch-undetectable
                                   (lambda _
                                     (substitute* "third_party/blink/renderer/core/frame/navigator.h"
                                                  (("return true;")
                                                   "return false;"))
                                     (substitute* "third_party/blink/renderer/core/frame/navigator.idl"
                                                  (("Navigator includes NavigatorAutomationInformation;")
                                                   "//Navigator includes NavigatorAutomationInformation;"))
                                     (substitute* "chrome/test/chromedriver/chrome/devtools_client_impl.cc"
                                                  (("cdc_adoQpoasnfa76pfcZLmcfl")
                                                   "barney_the_dinosaur"))
                                     (substitute* "chrome/test/chromedriver/js/test.js"
                                                  (("cdc_adoQpoasnfa76pfcZLmcfl")
                                                   "barney_the_dinosaur"))
                                     (substitute* "chrome/test/chromedriver/js/execute_async_script.js"
                                                  (("cdc_adoQpoasnfa76pfcZLmcfl")
                                                   "barney_the_dinosaur"))
                                     (substitute* "chrome/test/chromedriver/js/execute_script.js"
                                                  (("cdc_adoQpoasnfa76pfcZLmcfl")
                                                   "barney_the_dinosaur"))
                                     (substitute* "chrome/test/chromedriver/js/call_function.js"
                                                  (("cdc_adoQpoasnfa76pfcZLmcfl")
                                                   "barney_the_dinosaur"))
                                     #t))))))))
