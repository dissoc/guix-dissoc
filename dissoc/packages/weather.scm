(define-module (dissoc packages weather)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages base)
  #:use-module (gnu packages icu4c)
  #:use-module (gnu packages java)
  #:use-module (gnu packages linux)
  #:use-module (guix build utils)
  #:use-module (guix build-system ant)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix packages))

(define-public wct
  (package
   (name "wct")
   (version "4.5.0")
   (source
    (origin
     (method url-fetch/zipbomb)
     (uri (string-append "https://www.ncdc.noaa.gov/wct/app/dist/wct-"
                         version
                         "-src.zip"))
     (sha256
      (base32 "131q13mxilw5jsfkmz57rf8zr963dqw2hyzx26g24v76f9qs2d3y"))))
   (build-system ant-build-system)
   (arguments
    `(#:tests? #f
      #:jdk ,icedtea-8 ; doesn't build with JDK8+
      #:build-target "compile"
      #:phases
      (modify-phases %standard-phases
                     (add-after 'unpack 'convert-to-utf8
                                (lambda _
                                  (chdir "wct")
                                  (for-each
                                   (lambda (file-path)
                                     (make-file-writable file-path)
                                     ;; convert the file from ISO-8859-1 to UTF-8
                                     (invoke "uconv"
                                             "-f" "ISO-8859-1"
                                             "-t" "utf-8"
                                             "-o"  file-path
                                             file-path))
                                   (find-files "./src"))
                                  #t))
                     (replace 'install
                              (lambda _
                                (apply invoke '("ant" "dist-all"))
                                #f)))))
   (propagated-inputs
    `(("icu4c" ,icu4c)))
   (inputs
    `(("icedtea-8" ,icedtea-8)))
   (home-page "https://www.ncdc.noaa.gov/wct/")
   (synopsis  "NOAA's Weather and Climate Toolkit")
   (description "The WCT provides tools for background maps, animations and basic filtering. The export of images and movies is provided in multiple formats. The data export feature supports conversion of data to a variety of common formats including GeoJSON, KMZ, Shapefile, Well-Known Text, GeoTIFF, ESRI Grid and Gridded NetCDF. These data export features promote the interoperability of weather and climate information with various scientific communities and common software packages such as ArcGIS, Google Earth, MatLAB, QGIS, R and many more. Advanced data export support for Google Earth enables the 2-D and 3D export of rendered data and isosurfaces.")
   (license license:gpl3+)))
