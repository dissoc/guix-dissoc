(define-module (dissoc packages wildfly)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages base)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages gl)
  #:use-module (gnu packages glib)
  #:use-module (gnu packages gstreamer)
  #:use-module (gnu packages icu4c)
  #:use-module (gnu packages java)
  #:use-module (gnu packages jemalloc)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages python)
  #:use-module (gnu packages video)
  #:use-module (guix build utils)
  #:use-module (guix build-system copy)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix packages))

(define-public wildfly13
  (package
   (name "wildfly13")
   (version "13.0.0.Final")
   (source
    (origin
     (method url-fetch)
     (uri  (string-append "http://download.jboss.org/wildfly/"
                          version
                          "/wildfly-"
                          version
                          ".tar.gz"))
     (sha256
      (base32 "1ycpm40xrlvr3xgz2y4qydw59svyaba1xlj3zd86dz435a4a2kzp"))))
   (build-system copy-build-system)
   (arguments
    `(#:install-plan
      '(("./"  "/opt/wildfly"))
      #:phases
      (modify-phases
       %standard-phases
       (add-after 'install 'patch-bins
                  (lambda* (#:key inputs outputs #:allow-other-keys)
                    (let* ((out (assoc-ref outputs "out")))
                      (substitute* (string-append out "/opt/wildfly/bin/add-user.sh" )
                                   (("dirname")
                                    (string-append (assoc-ref inputs "coreutils")
                                                   "/bin/dirname"))
                                   (("basename")
                                    (string-append (assoc-ref inputs "coreutils")
                                                   "/bin/basename"))
                                   (("grep")
                                    (string-append (assoc-ref inputs "grep")
                                                   "/bin/grep"))
                                   (("uname")
                                    (string-append (assoc-ref inputs "coreutils")
                                                   "/bin/uname"))
                                   (("sleep")
                                    (string-append (assoc-ref inputs "coreutils")
                                                   "/bin/sleep"))
                                   (("readlink")
                                    (string-append (assoc-ref inputs "coreutils")
                                                   "/bin/readlink")))
                      (substitute* (string-append out "/opt/wildfly/bin/standalone.sh" )
                                   (("dirname")
                                    (string-append (assoc-ref inputs "coreutils")
                                                   "/bin/dirname"))
                                   (("basename")
                                    (string-append (assoc-ref inputs "coreutils")
                                                   "/bin/basename"))
                                   (("grep")
                                    (string-append (assoc-ref inputs "grep")
                                                   "/bin/grep"))
                                   (("uname")
                                    (string-append (assoc-ref inputs "coreutils")
                                                   "/bin/uname"))
                                   (("sleep")
                                    (string-append (assoc-ref inputs "coreutils")
                                                   "/bin/sleep"))
                                   (("tr")
                                    (string-append (assoc-ref inputs "coreutils")
                                                   "/bin/tr"))
                                   (("readlink")
                                    (string-append (assoc-ref inputs "coreutils")
                                                   "/bin/readlink")))
                      #t))))))
   (inputs
    `(("coreutils" ,coreutils)
      ("grep" ,grep)))
   (home-page "ttp://www.gradle.org/")
   (synopsis "WildFly is a powerful, modular, & lightweight application server that helps you build amazing applications.")
   (description
    "WildFly,[2] formerly known as JBoss AS, or simply JBoss, is an application server authored by JBoss, now developed by Red Hat. WildFly is written in Java and implements the Java Platform, Enterprise Edition (Java EE) specification. It runs on multiple platforms.")
   (license license:lgpl2.1)))
