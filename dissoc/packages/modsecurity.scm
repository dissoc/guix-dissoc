(define-module (dissoc packages modsecurity)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages autotools)
  #:use-module (gnu packages bash)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages curl)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages lua)
  #:use-module (gnu packages pcre)
  #:use-module (gnu packages python)
  #:use-module (gnu packages python-science)
  #:use-module (gnu packages python-xyz)
  #:use-module (gnu packages qt)
  #:use-module (gnu packages version-control)
  #:use-module (gnu packages web)
  #:use-module (gnu packages xml)
  #:use-module (gnu packages xorg)
  #:use-module (guix build union)
  #:use-module (guix build utils)
  #:use-module (guix build-system cmake)
  #:use-module (guix build-system gnu)
  #:use-module (guix build-system python)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix packages))

(define-public libmodsecurity
  (package
   (name "libmodsecurity")
   (version "3.0.4")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url "https://github.com/SpiderLabs/ModSecurity.git")
                  (commit (string-append "v" version))
                  (recursive? #t)))
            (sha256
             (base32
              "07vry10cdll94sp652hwapn0ppjv3mb7n2s781yhy7hssap6f2vp"))))
   (build-system gnu-build-system)
   (arguments
    `(#:configure-flags (list (string-append "--with-libxml="
                                             (assoc-ref %build-inputs
                                                        "libxml2"))
                              (string-append "--with-curl="
                                             (assoc-ref %build-inputs
                                                        "curl"))
                              (string-append "--with-lua="
                                             (assoc-ref %build-inputs
                                                        "lua"))
                              (string-append "--with-pcre="
                                             (assoc-ref %build-inputs
                                                        "pcre:bin")))))
   (native-inputs
    `(("autoconfig" ,autoconf)
      ("automake" ,automake)
      ("libtool" ,libtool)))
   (inputs
    `(("curl" ,curl)
      ("lua" ,lua)
      ("pcre:bin" ,pcre "bin")
      ("libxml2" ,libxml2)))
   (synopsis "ModSecurity v3 library component.")
   (description "Libmodsecurity is one component of the ModSecurity v3 project. The
      library codebase serves as an interface to ModSecurity Connectors taking
      in web traffic and applying traditional ModSecurity processing. In
      general, it provides the capability to load/interpret rules written in
      the ModSecurity SecRules format and apply them to HTTP content provided
      by your application via Connectors.")
   (license license:asl2.0)
   (home-page "https://www.modsecurity.org/")))

(define-public modsecurity-apache
  (package
   (name "modsecurity-apache")
   (version "master")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url "https://github.com/SpiderLabs/ModSecurity-apache.git")
                  (commit "master")))
            (sha256
             (base32
              "18an5fp1f985ma2yj86kmhbh2jj7vx01j6w55phgdwgqk4xmv4m1"))))
   (build-system gnu-build-system)
   (arguments
    `(#:configure-flags
      (list (string-append "--with-apxs="
                           (assoc-ref %build-inputs
                                      "httpd")
                           "/bin/apxs")
            (string-append "--with-libmodsecurity="
                           (assoc-ref %build-inputs
                                      "libmodsecurity"))
            (string-append "--with-apache="
                           (assoc-ref %build-inputs
                                      "httpd")
                           "/bin/httpd"))
      #:phases
      (modify-phases %standard-phases
                     (replace 'install
                              (lambda* (#:key outputs inputs #:allow-other-keys)
                                (let* ((out    (assoc-ref outputs "out")))
                                  ;; apxs - -n mod_security3 ./src/.libs/mod_security3.so fails
                                  (install-file "src/.libs/mod_security3.so"
                                                (string-append out "/modules/")))
                                #t)))))
   (native-inputs
    `(("autoconfig" ,autoconf)
      ("automake" ,automake)
      ("httpd" ,httpd)
      ("libtool" ,libtool)))
   (inputs
    `(("libmodsecurity" ,libmodsecurity)))
   (synopsis "ModSecurity v3 library component.")
   (description "Libmodsecurity is one component of the ModSecurity v3 project. The
      library codebase serves as an interface to ModSecurity Connectors taking
      in web traffic and applying traditional ModSecurity processing. In
      general, it provides the capability to load/interpret rules written in
      the ModSecurity SecRules format and apply them to HTTP content provided
      by your application via Connectors.")
   (license license:asl2.0)
   (home-page "https://www.modsecurity.org/")))

(define-public modsecurity-nginx
  (package
   (name "modsecurity-nginx")
   (version "1.0.1")
   (source (origin
            (method git-fetch)
            (uri (git-reference
                  (url "https://github.com/SpiderLabs/ModSecurity-nginx.git")
                  (commit (string-append "v" version))))
            (sha256
             (base32
              "0cbb3g3g4v6q5zc6an212ia5kjjad62bidnkm8b70i4qv1615pzf"))))
   (build-system gnu-build-system)
   (arguments
    `(#:configure-flags (list
                         (string-append "--add-dynamic-module=./modsecurity-nginx")
                         "--with-compat")
      #:phases
      (modify-phases %standard-phases
                     (replace 'unpack
                              (lambda* (#:key source #:allow-other-keys)
                                (display "source")
                                (display source)
                                (if (file-is-directory? source)
                                    (begin
                                      (mkdir "source")
                                      (chdir "source")
                                      (mkdir "modsecurity-nginx")
                                      (copy-recursively source "modsecurity-nginx"
                                                        #:keep-mtime? #t)))
                                #t))
                     (add-after 'unpack 'unpack-nginx-sources
                                (lambda* (#:key inputs native-inputs #:allow-other-keys)
                                  (begin
                                    ;; The nginx source code is part of the module’s source.
                                    (format #t "decompressing nginx source code~%")
                                    (let ((tar (assoc-ref inputs "tar"))
                                          (nginx-srcs (assoc-ref inputs "nginx-sources")))
                                      (invoke (string-append tar "/bin/tar")
                                              "xvf" nginx-srcs "--strip-components=1"))
                                    #t)))

                     (add-before 'configure 'patch-/bin/sh
                                 (lambda _
                                   (substitute* "auto/feature"
                                                (("/bin/sh") (which "sh")))
                                   #t))
                     (replace 'configure
                              (lambda* (#:key outputs inputs #:allow-other-keys)
                                (setenv "CC" "gcc")

                                (format #t "environment variable `CC' set to `gcc'~%")

                                (invoke "./configure"
                                        "--add-dynamic-module=./modsecurity-nginx"
                                        "--with-compat")
                                #t))
                     (delete 'check)
                     (replace 'install
                              (lambda* (#:key outputs inputs #:allow-other-keys)
                                (let* ((out (assoc-ref outputs "out"))
                                       (modules-dir (string-append out "/etc/nginx/modules")))
                                  (mkdir-p modules-dir)
                                  (copy-file "objs/ngx_http_modsecurity_module.so"
                                             (string-append
                                              modules-dir "/ngx_http_modsecurity_module.so"))
                                  #t))))))
   (native-inputs
    `(("autoconfig" ,autoconf)
      ("automake" ,automake)
      ("nginx" ,nginx)
      ("libtool" ,libtool)))
   (inputs
    `(("libmodsecurity" ,libmodsecurity)
      ("pcre" ,pcre)
      ("zlib" ,zlib)
      ("nginx-sources" ,(package-source nginx))))
   (synopsis "ModSecurity v3 library component.")
   (description "Libmodsecurity is one component of the ModSecurity v3 project. The
      library codebase serves as an interface to ModSecurity Connectors taking
      in web traffic and applying traditional ModSecurity processing. In
      general, it provides the capability to load/interpret rules written in
      the ModSecurity SecRules format and apply them to HTTP content provided
      by your application via Connectors.")
   (license license:asl2.0)
   (home-page "https://www.modsecurity.org/")))
