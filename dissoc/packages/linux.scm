(define-module (dissoc packages linux)
  #:use-module (gnu packages)
  #:use-module (gnu packages base)
  #:use-module (gnu packages compression)
  #:use-module (gnu packages linux)
  #:use-module (guix licenses)
  #:use-module (guix packages)
  #:use-module (guix utils)
  #:use-module (guix download)
  #:use-module (ice-9 match))

(define %default-hardend-linux-options
  `(("CONFIG_SECURITY" . #t)
    ("CONFIG_SECURITY_NETWORK" . #t)
    ("CONFIG_SECURITY_SELINUX" . #t)
    ("CONFIG_SECURITY_SELINUX_DEVELOP" . #t)
    ("SECURITY_SELINUX_BOOTPARAM" . #t)
    ("CONFIG_SECURITY_APPARMOR" . #f)
    ("CONFIG_SECURITY_TOMOYO" . #f)
    ("CONFIG_SECURITY_SMACK" . #f)))

(define (linux-urls version)
  "Return a list of URLS for Linux VERSION."
  (list (string-append "https://www.kernel.org/pub/linux/kernel/v"
                       (version-major version) ".x/linux-" version ".tar.xz")))

(define (harden-linux freedo version hash)
  (package
   (inherit freedo)
   (name "linux")
   (version version)
   (source (origin
            (method url-fetch)
            (uri (linux-urls version))
            (sha256 (base32 hash))))
   (home-page "https://www.kernel.org/")
   (synopsis "Linux kernel with nonfree binary blobs included")
   (description
    "The unmodified Linux kernel, including nonfree blobs, for running Guix
System on hardware which requires nonfree software to function.")))

(define-public linux-libre-hardened-5.10
  ((@@ (gnu packages linux) make-linux-libre*)
   linux-libre-5.10-version
   linux-libre-5.10-source
   '("x86_64-linux")
   #:configuration-file (@@ (gnu packages linux) kernel-config)
   #:extra-version "hardened"
   ;; extra options appends to .config
   #:extra-options (append  (@@ (gnu packages linux) %default-extra-linux-options)
                            %default-hardend-linux-options)))

(define-public linux-hardened-5.10
  (harden-linux linux-libre-hardened-5.10 "5.10.14"
                "0ahxga1jdgn8kxln0pn8j42qxx0dhrhm9vcpwilyjnwb36gvf9zs"))
