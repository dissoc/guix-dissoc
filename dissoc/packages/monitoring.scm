(define-module (dissoc packages monitoring)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (dissoc packages wildfly)
  #:use-module (gnu packages base)
  #:use-module (gnu packages java)
  #:use-module (gnu packages monitoring)
  #:use-module (gnu packages pcre)
  #:use-module (gnu packages)
  #:use-module (guix build-system gnu)
  #:use-module (guix download)
  #:use-module (guix packages)
  #:use-module (guix utils))

(define-public zabbix-jmx-agent
  (package
   (name "zabbix-jmx-agent")
   (version "5.2.2")
   (source
    (origin
     (method url-fetch)
     (uri (string-append
           "https://cdn.zabbix.com/zabbix/sources/stable/"
           (version-major+minor version) "/zabbix-" version ".tar.gz"))
     (sha256
      (base32 "16sqx5hrqkciwnl6xs1b8mwf0fz7x9f4214jhj9s86w0mqiscw8g"))))
   (build-system gnu-build-system)
   (arguments
    `(#:configure-flags
      (list "--enable-java")
      #:phases
      (modify-phases
       %standard-phases
       (add-after 'install 'patch-java
                  (lambda* (#:key inputs outputs #:allow-other-keys)
                    (let* ((out (assoc-ref outputs "out"))
                           (java-bin (string-append (assoc-ref inputs "openjdk:jdk")
                                                    "/bin/java")))
                      (substitute* (string-append out "/sbin/zabbix_java/lib/logback.xml" )
                                   (("/tmp/zabbix_java.log")
                                    "/var/log/zabbix/jmx-agent-service.log"))
                      (substitute* (string-append out "/sbin/zabbix_java/startup.sh" )
                                   (("cd `dirname ")
                                    (string-append "##cd `dirname"
                                                   "\n"
                                                   "cd "
                                                   (string-append out "/sbin/zabbix_java/")
                                                   "\n"
                                                   "###"))
                                   ((". ./settings.sh")
                                    "##. ./settings.sh")
                                   (("# verify that the gateway started successfully")
                                    "echo $COMMAND_LINE")
                                   (("cat")
                                    (string-append (assoc-ref inputs "coreutils")
                                                   "/bin/cat"))
                                   (("rm -f")
                                    (string-append (assoc-ref inputs "coreutils")
                                                   "/bin/rm -f"))
                                   (("touch")
                                    (string-append (assoc-ref inputs "coreutils")
                                                   "/bin/touch")))
                      (wrap-program (string-append out "/sbin/zabbix_java/startup.sh")
                                    `("JAVA" ":" = (,java-bin)))
                      #t)))
       (add-after 'patch-java 'copy-wildfly-client
                  (lambda* (#:key inputs outputs #:allow-other-keys)
                    (let* ((out (assoc-ref outputs "out"))
                           (client (string-append (assoc-ref inputs "wildfly13")
                                                  "/opt/wildfly/bin/client/jboss-client.jar")))
                      (install-file client (string-append out "/sbin/zabbix_java/lib/"))
                      #t))))))
   (native-inputs
    `(("wildfly13" ,wildfly13)))
   (inputs
    `(("openjdk:jdk" ,openjdk14 "jdk")
      ("coreutils" ,coreutils)))
   (home-page "https://www.zabbix.com/")
   (synopsis "Zabbix jmx agent built to work with wildfly")
   (description "This package provides a distributed monitoring
solution (client-side agent)")
   (license license:gpl2)))
