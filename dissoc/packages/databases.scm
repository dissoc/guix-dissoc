(define-module (dissoc packages databases)
  #:use-module ((guix licenses) #:prefix license:)
  #:use-module (gnu packages base)
  #:use-module (gnu packages icu4c)
  #:use-module (gnu packages java)
  #:use-module (gnu packages jemalloc)
  #:use-module (gnu packages linux)
  #:use-module (gnu packages python)
  #:use-module (guix build utils)
  #:use-module (guix build-system copy)
  #:use-module (guix download)
  #:use-module (guix git-download)
  #:use-module (guix packages))

(define-public cassandra
  (package
   (name "cassandra")
   (version "3.11.10")
   (source
    (origin
     (method url-fetch)
     (uri  (string-append
            "https://mirrors.ocf.berkeley.edu/apache/cassandra/"
            version
            "/apache-cassandra-"
            version
            "-bin.tar.gz"))
     (sha256
      (base32 "1wcv0drhb765fda6kkpsxsyfdv4cqf7nqfwc4bimh4c4djap5rxv"))))
   (build-system copy-build-system)
   (arguments
    `(#:install-plan
      '(("." "/opt/cassandra/")
        ("bin/cassandra" "bin/")
        ("bin/cqlsh" "bin/")
        ("bin/nodetool" "bin/"))
      #:phases
      (modify-phases
       %standard-phases
       (add-after 'install 'wrap-cassandra
                  (lambda* (#:key inputs outputs #:allow-other-keys)
                    (let* ((out (assoc-ref outputs "out"))
                           (opt (string-append out "/opt/cassandra"))
                           (data-directory "/var/lib/cassandra")
                           (log-directory "/var/lib/cassandra/log/")
                           (jemalloc-dir (string-append (assoc-ref inputs "jemalloc")
                                                        "/lib/libjemalloc.so"))
                           (python3-dir (string-append (assoc-ref inputs "python")
                                                       "/bin/python3")))
                      (substitute* (string-append out "/opt/cassandra/bin/cassandra.in.sh")
                                   (("\\$CASSANDRA_HOME/data")
                                    data-directory))
                      (wrap-program (string-append out "/bin/cassandra")
                                    `("JAVA_HOME" ":" = (,(assoc-ref inputs "icedtea")))
                                    `("CASSANDRA_INCLUDE" ":" = (,(string-append opt "/bin/cassandra.in.sh")))
                                    `("CASSANDRA_HOME" ":" = (,opt))
                                    `("CASSANDRA_LOG_DIR" ":" = (,log-directory))
                                    `("CASSANDRA_HOME" ":" = (,opt))
                                    `("CASSANDRA_LIBJEMALLOC" ":" = (,jemalloc-dir)))
                      (substitute* (string-append out "/bin/cqlsh")
                                   (("python")
                                    python3-dir)))
                    #t)))))
   (inputs
    `(("icedtea" ,icedtea-8)
      ("jemalloc" ,jemalloc)
      ("numactl" ,numactl)
      ("python" ,python)))
   (home-page "https://cassandra.apache.org/")
   (synopsis "Distributed database")
   (description "Apache Cassandra is a free and open-source, distributed, wide column store, NoSQL database management system designed to handle large amounts of data across many commodity servers, providing high availability with no single point of failure.")
   (license license:asl2.0)))
