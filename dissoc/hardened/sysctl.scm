(define-module (dissoc hardened sysctl))

;; TODO: add detailed comments for each parameter

(define-public %kernel-self-protection
  ;;https://madaidans-insecurities.github.io/guides/linux-hardening.html#sysctl
  '(;; This toggle indicates whether restrictions are placed on exposing kernel
    ;; addresses via /proc and other interfaces.
    ;; When kptr_restrict is set to (2), kernel pointers printed using %pK will
    ;; be replaced with 0’s regardless of privileges.
    ;; https://sysctl-explorer.net/kernel/kptr_restrict/
    ("kernel.kptr_restrict" . "2")

    ;; This toggle indicates whether unprivileged users are prevented from using
    ;; dmesg(8) to view messages from the kernel’s log buffer.
    ;; https://sysctl-explorer.net/kernel/dmesg_restrict/
    ("kernel.dmesg_restrict" . "1")


    ;; The four values in printk denote: console_loglevel,
    ;; default_message_loglevel, minimum_console_loglevel and
    ;; default_console_loglevel respectively.
    ;; https://sysctl-explorer.net/kernel/printk/
    ("kernel.printk" . "3 3 3 3")

    ;; disable unprivileged access to bpf() entirely.
    ;; https://lwn.net/Articles/660331/
    ("kernel.unprivileged_bpf_disabled" . "1")

    ;; This enables hardening for the BPF JIT compiler. Supported are eBPF JIT
    ;; backends. Enabling hardening trades off performance, but can mitigate JIT
    ;; spraying. Values : 0 - disable JIT hardening (default value) 1 - enable
    ;; JIT hardening for unprivileged users only 2 - enable JIT hardening for
    ;; all users
    ("net.core.bpf_jit_harden" . "2")

    ;; Historically the kernel has always automatically loaded any
    ;; line discipline that is in a kernel module when a user asks
    ;; for it to be loaded with the TIOCSETD ioctl, or through other
    ;; means.  This is not always the best thing to do on systems
    ;; where you know you will not be using some of the more
    ;; "ancient" line disciplines, so prevent the kernel from doing
    ;; this unless the request is coming from a process with the
    ;; CAP_SYS_MODULE permissions.
    ;; drivers/tty/serial/Kconfig
    ("dev.tty.ldisc_autoload" . "0")

    ;; - When set to "disabled", all unprivileged users are forbidden to
    ;;   use userfaultfd syscalls.

    ;; - When set to "enabled", all users are allowed to use userfaultfd
    ;;   syscalls.

    ;; - When set to "kvm", all unprivileged users are forbidden to use the
    ;;   userfaultfd syscalls, except the user who has permission to open
    ;;   /dev/kvm.

    ;; This new flag can add one more layer of security to reduce the attack
    ;; surface of the kernel by abusing userfaultfd.  Here we grant the
    ;; thread userfaultfd permission by checking against CAP_SYS_PTRACE
    ;; capability.  By default, the value is "disabled" which is the most
    ;; strict policy.  Distributions can have their own perferred value.

    ;; The "kvm" entry is a bit special here only to make sure that existing
    ;; users like QEMU/KVM won't break by this newly introduced flag.  What
    ;; we need to do is simply set the "unprivileged_userfaultfd" flag to
    ;; "kvm" here to automatically grant userfaultfd permission for processes
    ;; like QEMU/KVM without extra code to tweak these flags in the admin
    ;; code.
    ;; https://lwn.net/Articles/782745/
    ("vm.unprivileged_userfaultfd" . "0")

    ;; A toggle indicating if the kexec_load syscall has been disabled. This
    ;; value defaults to 0 (false: kexec_load enabled), but can be set to 1
    ;; (true: kexec_load disabled). Once true, kexec can no longer be used, and the
    ;; toggle cannot be set back to false. This allows a kexec image to be loaded
    ;; before disabling the syscall, allowing a system to set up (and later use) an
    ;; image without it being altered. Generally used together with the
    ;; “modules_disabled” sysctl.

    ;; kexec is a system call that is used to boot another kernel during runtime.

    ;; https://sysctl-explorer.net/kernel/kexec_load_disabled/
    ("kernel.kexec_load_disabled" . "1")

    ;; What is the magic SysRq key?
    ;; It is a ‘magical’ key combo you can hit which the kernel will respond
    ;; to regardless of whatever else it is doing, unless it is completely
    ;; locked up.

    ;; 0 - disable sysrq completely

    ;; 1 - enable all functions of sysrq

    ;; >1 - bitmask of allowed sysrq functions (see below for detailed function
    ;; description):

    ;; 2 =   0x2 - enable control of console logging level
    ;; 4 =   0x4 - enable control of keyboard (SAK, unraw)
    ;; 8 =   0x8 - enable debugging dumps of processes etc.
    ;; 16 =  0x10 - enable sync command
    ;; 32 =  0x20 - enable remount read-only
    ;; 64 =  0x40 - enable signalling of processes (term, kill, oom-kill)
    ;; 128 =  0x80 - allow reboot/poweroff
    ;; 256 = 0x100 - allow nicing of all RT tasks
    ;; https://www.kernel.org/doc/html/v4.11/admin-guide/sysrq.html
    ("kernel.sysrq" . "4")

    ;; User namespaces are a feature in the kernel which aim to improve
    ;; sandboxing and make it easily accessible for unprivileged users however,
    ;; this feature exposes significant kernel attack surface for privilege
    ;; escalation so this sysctl restricts the usage of user namespaces to the
    ;; CAP_SYS_ADMIN capability. For unprivileged sandboxing, it is instead
    ;; recommended to use a setuid binary with little attack surface to minimize
    ;; the potential for privilege escalation. This topic is covered further in
    ;; the sandboxing section.
    ;; Be aware though that this sysctl only exists on certain Linux
    ;; distributions as it requires a kernel patch. If your kernel does not
    ;; include this patch, you can alternatively disable user namespaces
    ;; completely (including for root) by setting user.max_user_namespaces=0.
    ;; https://madaidans-insecurities.github.io/guides/linux-hardening.html#sysctl
    ;;("kernel.unprivileged_userns_clone" . "0")

    ;; Performance events add considerable kernel attack surface and have caused
    ;;abundant vulnerabilities. This sysctl restricts all usage of performance
    ;; events to the CAP_PERFMON capability (CAP_SYS_ADMIN on kernel versions
    ;; prior to 5.8).

    ;; Be aware that this sysctl also requires a kernel patch that is only
    ;; available on certain distributions. Otherwise, this setting is equivalent
    ;; to kernel.perf_event_paranoid=2 which only restricts a subset of this
    ;; functionality.
    ;; https://madaidans-insecurities.github.io/guides/linux-hardening.html#sysctl
    ("kernel.perf_event_paranoid" . "3")))

(define %network
  '(;; This helps protect against SYN flood attacks which are a form of denial
    ;; of service attack in which an attacker sends a large amount of bogus SYN
    ;; requests in an attempt to consume enough resources to make the system
    ;; unresponsive to legitimate traffic.
    ("net.ipv4.tcp_syncookies" . "1")

    ;; This protects against time-wait assassination by dropping RST packets for
    ;; sockets in the time-wait state.
    ("net.ipv4.tcp_rfc1337=1" . "1")

    ;; These enable source validation of packets received from all interfaces of
    ;; the machine. This protects against IP spoofing in which an attacker sends
    ;; a packet with a fraudulent IP address.
    ("net.ipv4.conf.all.rp_filter" . "1")
    ("net.ipv4.conf.default.rp_filter" . "1")

    ;;These disable ICMP redirect acceptance and sending to prevent
    ;; man-in-the-middle attacks and minimize information disclosure.
    ("net.ipv4.conf.all.accept_redirects" . "0")
    ("net.ipv4.conf.default.accept_redirects" . "0")
    ("net.ipv4.conf.all.secure_redirects" . "0")
    ("net.ipv4.conf.default.secure_redirects" . "0")
    ("net.ipv6.conf.all.accept_redirects" . "0")
    ("net.ipv6.conf.default.accept_redirects" . "0")
    ("net.ipv4.conf.all.send_redirects" . "0")
    ("net.ipv4.conf.default.send_redirects" . "0")

    ;; TODO: check ipv6 equivalents
    ("net.ipv4.icmp_echo_ignore_all" . "1")
    ("net.ipv6.icmp.echo_ignore_all" . "1")

    ("net.ipv4.conf.all.accept_source_route" . "0")
    ("net.ipv4.conf.default.accept_source_route" . "0")
    ("net.ipv6.conf.all.accept_source_route" . "0")
    ("net.ipv6.conf.default.accept_source_route" . "0")

    ("net.ipv6.conf.all.accept_ra" . "0")
    ("net.ipv6.conf.default.accept_ra" . "0")

    ("net.ipv4.tcp_sack" . "0")
    ("net.ipv4.tcp_dsack" . "0")
    ("net.ipv4.tcp_fack" . "0")))

(define-public %user-space
  '(("kernel.yama.ptrace_scope" . "2")
    ("vm.mmap_rnd_bits" . "32")
    ("vm.mmap_rnd_compat_bits" . "16")
    ("fs.protected_symlinks" . "1")
    ("fs.protected_hardlinks" . "1")
    ("fs.protected_fifos" . "2")
    ("fs.protected_regular" . "2")))

(define-public %hardened-sysctls (append %kernel-self-protection
                                  %network
                                  %user-space))
