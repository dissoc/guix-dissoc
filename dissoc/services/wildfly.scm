(define-module (dissoc services wildfly)
  #:use-module (dissoc packages wildfly)
  #:use-module (gnu packages admin)
  #:use-module (gnu packages java)
  #:use-module (gnu services admin)
  #:use-module (gnu services base)
  #:use-module (gnu services configuration)
  #:use-module (gnu services shepherd)
  #:use-module (gnu services)
  #:use-module (gnu system shadow)
  #:use-module (guix gexp)
  #:use-module (guix packages)
  #:use-module (guix records)
  #:export (wildfly-service-type
            wildfly-configuration))

(define-record-type* <wildfly-configuration>
  wildfly-configuration make-wildfly-configuration
  wildfly-configuration?
  (config-files-dir wildfly-configuration-config-files-dir
                    (default '()))
  (config-dir wildfly-configuration-config-dir
              (default "/var/lib/wildfly/config/"))
  (temp-dir wildfly-configuration-temp-dir
            (default "/var/lib/wildfly/temp/"))
  (deployment-dir wildfly-configuration-deployment-dir
                  (default "/var/lib/wildfly/deployment/"))
  (log-dir wildfly-configuration-log-dir
           (default "/var/lib/wildfly/log/"))
  (data-dir wildfly-configuration-data-dir
            (default "/var/lib/wildfly/data/"))
  (server-type wildfly-configuration-server-type
               (default "standalone"))
  (bind-address wildfly-configuration-bind-address
                (default "127.0.0.1"))
  (bind-address-management wildfly-configuration-bind-address-management
                           (default "127.0.0.1"))
  (wildfly wildfly-configuration-wildfly
           (default wildfly13)))

(define %wildfly-log-rotations
  (list (log-rotation
         (files (list "/var/log/wildfly.log"))
         (options '("rotate 7"))
         (frequency 'daily))))

(define (wildfly-activation config)
  "Return the activation gexp for CONFIG."
  #~(begin
      (use-modules (guix build utils)
                   (ice-9 ftw))
      (let* ((config-dir #$(wildfly-configuration-config-dir config))
             (config-files-dir #$(wildfly-configuration-config-files-dir config))
             (temp-dir #$(wildfly-configuration-temp-dir config))
             (deployment-dir #$(wildfly-configuration-deployment-dir config))
             (log-dir #$(wildfly-configuration-log-dir config))
             (data-dir #$(wildfly-configuration-data-dir config))
             (pw  (getpwnam "wildfly"))
             (uid (passwd:uid pw))
             (gid (passwd:gid pw)))
        (mkdir-p config-dir)
        (chown config-dir uid gid)
        (mkdir-p temp-dir)
        (chown temp-dir uid gid)
        (mkdir-p deployment-dir)
        (chown deployment-dir uid gid)
        (mkdir-p log-dir)
        (chown log-dir uid gid)
        (mkdir-p data-dir)
        (chown data-dir uid gid)
        (copy-recursively  config-files-dir
                           config-dir)
        (for-each (lambda (f)
                    (chown (string-append config-dir f)
                           uid
                           gid)
                    (chmod (string-append config-dir f) #o700))
                  (cdr (cdr (scandir config-dir)))))))

(define (wildfly-account config)
  "Return the user accounts and user groups for CONFIG."
  (list (user-group (name "wildfly") (system? #t))
        (user-account
         (name "wildfly")
         (system? #t)
         (group "wildfly")
         (comment "wildfly privilege separation user")
         (home-directory (string-append "/var/empty"))
         (shell (file-append shadow "/sbin/nologin")))))

(define (wildfly-shepherd-service config)
  (let* ((wildfly (wildfly-configuration-wildfly config))
         (server-type (wildfly-configuration-server-type config))
         )
    (list (shepherd-service
           (documentation "Runs the wildfly app server")
           (provision '(wildfly))
           (start #~(make-forkexec-constructor
                     (list (string-append #$wildfly "/opt/wildfly/bin/standalone.sh")
                           (string-append "--read-only-server-config="
                                          #$(wildfly-configuration-config-dir config)
                                          "standalone.xml"))
                     #:log-file "/var/log/wildfly.log"
                     #:user "wildfly"
                     #:group "wildfly"
                     #:environment-variables
                     (list
                      (string-append "JAVA_OPTS="
                                     (string-join
                                      '("-Xms64m -Xmx512m -XX:MetaspaceSize=96M"
                                        "-XX:MaxMetaspaceSize=256m -Djava.net.preferIPv4Stack=true"
                                        "-Djboss.modules.system.pkgs=$JBOSS_MODULES_SYSTEM_PKGS"
                                        "-Djava.awt.headless=true"
                                        "--add-modules java.se"
                                        #$(string-append "-Djboss.server.log.dir="
                                                         (wildfly-configuration-log-dir config))
                                        #$(string-append "-Djboss.server.data.dir="
                                                         (wildfly-configuration-data-dir config))
                                        #$(string-append "-Djboss.server.temp.dir="
                                                         (wildfly-configuration-temp-dir config))
                                        #$(string-append "-Djboss.server.deploy.dir="
                                                         (wildfly-configuration-deployment-dir config))
                                        #$(string-append "-Djboss.server.config.dir="
                                                         (wildfly-configuration-config-dir config))
                                        "-Djboss.config.current-history-length=0"
                                        "-Djboss.config.history-days=0"
                                        "-Dlogging.configuration=file:/var/lib/wildfly/config/logging.properties"
                                        #$(string-append "-Djboss.bind.address.management="
                                                         (wildfly-configuration-bind-address-management config))
                                        #$(string-append "-Djboss.bind.address="
                                                         (wildfly-configuration-bind-address config)))))
                      ;; these variables are found in standalone.sh or domain.sh
                      (string-append "JBOSS_CONFIG_DIR=/var/lib/wildfly/config/")
                      (string-append "JBOSS_LOG_DIR=/var/lib/wildfly/log/")
                      (string-append "JAVA_HOME=" #$openjdk14:jdk)
                      (string-append "JBOSS_HOME=" #$wildfly "/opt/wildfly"))))
           (stop #~(make-kill-destructor))))))

(define wildfly-service-type
  (service-type (name 'wildfly)
                (description "Runs the wildfly app server")
                (extensions
                 (list
                  (service-extension shepherd-root-service-type
                                     wildfly-shepherd-service)
                  (service-extension account-service-type
                                     wildfly-account)
                  (service-extension activation-service-type
                                     wildfly-activation)
                  (service-extension rottlog-service-type
                                     (const %wildfly-log-rotations))))
                (default-value
                  (wildfly-configuration))))
